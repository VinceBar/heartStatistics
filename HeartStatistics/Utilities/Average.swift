//
//  Average.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

//import Foundation

class Average {
    
    func average(nums: [Double]) -> Double {
        
        var total = 0.0
        //use the parameter-array instead of the global variable votes
        for vote in nums{
            
            total += Double(vote)
            
        }
        
        let votesTotal = Double(nums.count)
        let average = total/votesTotal
        return average
    }
}
