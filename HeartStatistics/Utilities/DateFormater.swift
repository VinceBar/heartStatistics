//
//  DateFormater.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation

class DateFormater {
    
    func stingToDate (stringDate: String) -> Date {
        let format        = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateToReturn = format.date(from: stringDate) 
        return dateToReturn!
        
    }
    
}
