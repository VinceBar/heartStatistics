//
//  HealhtKitSetupAssistant.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import HealthKit

class HealthKitSetupAssistant {
    
    private enum HealthkitSetupError: Error {
        case notAvailableOnDevice
        case dataTypeNotAvailable
    }
    
    class func authorizeHealthKit(completion: @escaping (Bool, Error?) -> Swift.Void) {
        
        //1. Check to see if HealthKit Is Available on this device
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, HealthkitSetupError.notAvailableOnDevice)
            print("HealthKit n'est pas dispo !") // voir pour mettre un msg d'alerte ?
            return
        }
        
        //2. Prepare the data types that will interact with HealthKit
        guard let heartRate = HKObjectType.quantityType(forIdentifier: .heartRate)
                else {
                completion(false, HealthkitSetupError.dataTypeNotAvailable)
                return
        }
        
        //3. Prepare a list of types you want HealthKit to read and write
        let healthKitTypesToWrite: Set<HKSampleType> = [heartRate]
        
        let healthKitTypesToRead: Set<HKObjectType> = [heartRate]
        
        //4. Request Authorization
        HKHealthStore().requestAuthorization(toShare: healthKitTypesToWrite,
                                             read: healthKitTypesToRead) { (success, error) in
                                                completion(success, error)
        }
    }
}


