//
//  HeartDataManager.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import HealthKit

class HeartDataManager {
    
    func getHeartBPM(startDate: Date, endDate: Date, sampleType: HKSampleType , completion: (([HKSample]?, Error?) -> Void)!) {
        
        //Predicate
       
        let heartRateType:HKQuantityType   = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: HKQueryOptions.strictEndDate)
        
        //descriptor
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        //Query
        let heartRateQuery = HKSampleQuery(sampleType: heartRateType,
                                           predicate: predicate,
                                           limit: HKObjectQueryNoLimit,
            sortDescriptors: [sortDescriptor])
        { (query:HKSampleQuery, results:[HKSample]?, error:Error?) -> Void in
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            
            let heartBPMs = results!
            
            if completion != nil {
                completion(heartBPMs, nil)
            }
            
        }//eo-query
        
        return HKHealthStore().execute(heartRateQuery)
        
        
    }
    
}



