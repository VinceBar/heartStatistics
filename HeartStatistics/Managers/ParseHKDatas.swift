//
//  ParseHKDatas.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

//import Foundation
import HealthKit


struct ParseHKDatas {
    
    func sortHeartDatas(results:[HKSample]?) -> [Date: Double] {
        
        let heartRateUnit:HKUnit = HKUnit(from: "count/min")
        var heartDatas = [Date: Double]()
        
        guard (results?.count)! > 0 else {
            return heartDatas
        }
        for iter in 0...(results!.count-1)
        {
            guard let currData:HKQuantitySample = results![iter] as? HKQuantitySample else { return heartDatas }
            
            let dataDate = currData.startDate
            let bpm = currData.quantity.doubleValue(for: heartRateUnit)
            
            heartDatas[dataDate] = bpm
            
        }//eofl
        return heartDatas
    }
    
    
}
