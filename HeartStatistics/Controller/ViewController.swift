//
//  ViewController.swift
//  HeartStatistics
//
//  Created by bwc on 07/09/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import UIKit
import HealthKit

class ViewController: UIViewController {

    @IBOutlet weak var minBpmLabel: UILabel!
    @IBOutlet weak var maxBpmLabel: UILabel!
    @IBOutlet weak var averageBpmLabel: UILabel!
    @IBOutlet weak var hkPermissionsButtonOutlet: UIButton!
    
    let permissionsKey = "permissionsKey"
    
    var bpmMax: Double = 0.00
    var bpmMin: Double = 0.00
    var bpmAverage: Double = 0.00
    
    let dateDeDebut = "2017-01-26T12:39:00Z"
    let dateDeFin = "2018-08-29T12:39:00Z"
    
    var startDate = Date()
    var endDate = Date()
    
    var heartDatas = [Date: Double]()
    var heartDatasSorted = [(key:Date,value:Double)]()
    var bpm: [Double] = []
    var bpmDates: [Date] = []
    
    let heartDataManager = HeartDataManager()
    let average = Average()
    let parseHKDatas = ParseHKDatas()
    let dateFormater = DateFormater()
    let defaults = UserDefaults() // pour les cles de préférence utilisateur
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        startDate = dateFormater.stingToDate(stringDate: dateDeDebut)
        endDate = dateFormater.stingToDate(stringDate: dateDeFin)
        
        // On teste si on a la permission pour lire des données
        if let permKey = defaults.string(forKey: permissionsKey) {
            guard permKey == "Ok" else {
                return
            }
            hkPermissionsButtonOutlet.isHidden = true
            loadDatas()
            
        }
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.maxBpmLabel.text = "Bpm max : " + String(format: "%.0f", bpmMax)
        self.minBpmLabel.text = "Bpm min : " + String(format: "%.0f", bpmMin)
        self.averageBpmLabel.text = "Bpm Moy : " + String(format: "%.2f", bpmAverage)
        
    }


    @IBAction func hkPermissionsButton(_ sender: UIButton) {
        HealthKitSetupAssistant.authorizeHealthKit { (authorized, error) in
            
            guard authorized else {
                
                let baseMessage = "HealthKit Authorization Failed"
                
                if let error = error {
                    print("\(baseMessage). Reason: \(error.localizedDescription)")
                } else {
                    print(baseMessage)
                }
                
                return
            }
            
            // HealthKit Successfully Authorized
            self.defaults.set("Ok", forKey: self.permissionsKey)
            self.hkPermissionsButtonOutlet.isHidden = true
            self.loadDatas()
            
        }
    }

        func loadDatas() {
            
            // Create the HKSample for heartRate.
            
            let heartSample:HKQuantityType   = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
            
            DispatchQueue.main.async {
                
                // Call HeartDataManager's getHeartBPM() method to get the user's heart Datas.
                
                self.heartDataManager.getHeartBPM(startDate: self.startDate, endDate: self.endDate, sampleType: heartSample) { (heartResults, error) in
                    if( error != nil ) {
                        print("Error: \(String(describing: error?.localizedDescription))")
                        return
                    }
                    
                    // Parse heart datas
                    self.heartDatas = self.parseHKDatas.sortHeartDatas(results: heartResults)
                    
                    let datasSorted = self.heartDatas.sorted(by: {$0.0 < $1.0})
                    print(type(of:datasSorted))
                    self.heartDatasSorted = datasSorted
                    for (key, value) in datasSorted {
                        self.bpm.append(value)
                        self.bpmDates.append(key)
                    }
                    
                    guard self.bpm.count > 0 else {
                        print("Aucune donnée sur la période définie")
                        
                        let alert = UIAlertController(title: "Erreur", message: "Aucune donnée sur la période définie", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                        return
                        
                    }
                    
                    self.bpmMax = self.bpm.max()!
                    self.bpmMin = self.bpm.min()!
                    self.bpmAverage = self.average.average(nums: self.bpm)
                    
                    self.viewWillAppear(true)
                    
                }
            }
            
        }

}

